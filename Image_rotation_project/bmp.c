#include "bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "input.h"
#include "output.h"


#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");

#define FOR_BMP_HEADER( FOR_FIELD ) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD( t, n ) t n ;

#pragma pack(push, 1)
static struct bmp_header
{
    FOR_BMP_HEADER(DECLARE_FIELD)
};
#pragma pack(pop)

static struct bmp_header header_create(const struct image img) {
    struct bmp_header h = {0};
    h.bfType = 0x4d42;
    h.bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel) * img.width + padding_find(img) * img.height;
    h.bfReserved = 0;
    h.bOffBits = sizeof(struct bmp_header);
    h.biSize = 40;
    h.biWidth = img.width;
    h.biHeight = img.height;
    h.biPlanes = 1;
    h.biBitCount = 24;
    h.biCompression = 0;
    h.biSizeImage = img.width* img.height*sizeof(struct pixel);
    h.biXPelsPerMeter = 0;
    h.biYPelsPerMeter = 0;
    h.biClrUsed = 0;
    h.biClrImportant = 0;
    return h;
}

static size_t padding_find(const struct image img) {
    size_t padding = 0;
    if ((img.width * 3) % 4) padding = 4 - (img.width * 3) % 4;
    return padding;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header h = { 0 };
    fread(&h, sizeof(struct bmp_header), 1, in);
    if (!h) return READ_INVALID_HEADER;
    img->height = h.biHeight;
    img->width = h.biWidth;

    struct pixel* pixels = malloc(sizeof(struct pixel) * h.biHeight * h.biWidth);

    size_t padding = padding_find(*img);

    size_t count = 0;
    fseek(in, sizeof(struct bmp_header), SEEK_SET);
    for (uint32_t hgt = 0; hgt < h.biHeight; hgt++) {
        count = count + (fread(pixels + h.biWidth * hgt, sizeof(struct pixel), h.biWidth, in));
        fseek(in, padding, SEEK_CUR);
    }

    if (count != h.biWidth * h.biHeight) {
        free(pixels);
        return READ_INVALID_SIGNATURE;
    }

    img->data = pixels;
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = header_create(*img);
    fwrite(&header, sizeof(struct bmp_header), 1, out);

    uint8_t padding = 0;
    for(uint32_t i = 0; i < header.biHeight; i++) {
        if(fwrite(img->data+i*header.biWidth, sizeof(struct pixel), header.biWidth, out) < header.biWidth) 
            return  WRITE_IMAGE_ERROR;
        if (header.biWidth % 4 != 0)
            for (uint32_t pad = 0; pad < 4 - header.biWidth * 3 % 4; pad++)
                fwrite(&padding, sizeof(uint8_t), 1, out);
    }
    return WRITE_OK;

}
