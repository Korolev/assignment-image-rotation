#ifndef IMAGE_H
#define IMAGE_H


#include <inttypes.h>


struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel { uint8_t b, g, r; };

struct image image_rotate(const struct image img);

#endif // !IMAGE_H