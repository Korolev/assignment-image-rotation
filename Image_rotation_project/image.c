#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>

#include "image.h"
#include "util.h"


struct image image_rotate(const struct image img) {
    const size_t wdh = (const size_t)img.height;
    const size_t hgt = (const size_t)img.width;

    struct pixel* pixels = malloc(wdh * hgt *sizeof(struct pixel));

    for (size_t i = 0; i < wdh * hgt; i++) {
        const uint64_t row = i / img.width;
        const uint64_t newColumn = img.height - row - 1;
        const uint64_t newRow = i % img.width;
        pixels[newRow * img.height + newColumn] = img.data[i];
    }
    struct image new_img = { wdh, hgt, pixels };
    return new_img;
}
