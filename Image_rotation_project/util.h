#ifndef _UTIL_H_
#define _UTIL_H_

_Noreturn void err( const char* msg, ... );

void free_images(struct image* image, struct image* new_image);

#endif
