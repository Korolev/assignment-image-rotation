#include <stdio.h>

#include "image.h"
#include "bmp.h"
#include "input.h"




enum read_status file_image_convert(struct image* image, char* filename) {
    FILE*  file = fopen(filename, "r+b");
    if (!file) return READ_INVALID_FILENAME;
    enum read_status read_status = from_bmp(file, image);
    fclose(file);
    switch (read_status) {
        case(READ_OK)
            return READ_OK;
        case (READ_INVALID_HEADER):
            return READ_INVALID_HEADER;
        case(READ_INVALID_FILENAME):
            return READ_INVALID_FILENAME;
        case(READ_INVALID_SIGNATURE):
            return READ_INVALID_SIGNATURE;
    }
}
