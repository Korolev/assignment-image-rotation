#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "util.h"
#include "image.h"
#include "input.h"
#include "output.h"



#define BMP_FILE_NAME "img.bmp"
#define NEW_BMP_FILE_NAME "Rotated.bmp"

void usage() {
    fprintf(stderr, "Usage: ./print_header %s\n", BMP_FILE_NAME);
}


int main( int argc, char** argv ) {
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments \n" );
    if (argc > 2) err("Too many arguments \n" );


    char* filename = argv[1];
    char* new_filename = NEW_BMP_FILE_NAME;

    

    struct image image = { 0 };
    switch (file_image_convert(&image, filename)) {
    case (READ_OK):
        break;
    case (READ_INVALID_FILENAME):
        err("Invalid filename.\n");
    case (READ_INVALID_HEADER):
        err("Invalid header.\n");
    case (READ_INVALID_SIGNATURE):
        err("Invalid signature.\n");
    }
    
    struct image new_image = image_rotate(image);
    
    enum write_status write_status = image_file_convert(new_image, new_filename);
    free_images(&image, &new_image);
    switch (write_status) {
    case (WRITE_OK):
        break;
    case (WRITE_FILE_ERROR):
        err("Can't write file with this filename.\n");
    case (WRITE_ERROR):
        err("Invalid header.\n");
    case(WRITE_IMAGE_ERROR):
        err("Can't write image.\n");
    }
    printf("Rotate was completed successfully.\n");

    return 0;
}
