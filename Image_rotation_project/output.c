#include <stdio.h>

#include "image.h"
#include "output.h"
#include "bmp.h"
#include "util.h"


enum write_status image_file_convert(const struct image new_image, const char* const new_filename) {
    FILE* new_file = fopen(new_filename, "w+b");
    if (new_file == NULL) return WRITE_FILE_ERROR;
    enum write_status write_status = to_bmp(new_file, &new_image);
    fclose(new_file);
    switch (write_status) {
        case(WRITE_OK):
            break;
        case(WRITE_ERROR): 
            return WRITE_ERROR;
        case(WRITE_IMAGE_ERROR):
            return WRITE_IMAGE_ERROR;
    }
    return WRITE_OK;
}
