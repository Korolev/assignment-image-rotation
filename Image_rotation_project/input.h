#ifndef INPUT_H
#define INPUT_H


#include <stdio.h>

#include "image.h"

/*  deserializer   */
enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_FILENAME
};

enum read_status file_image_convert(struct image* image, char* filename);

#endif // !INPUT_H
