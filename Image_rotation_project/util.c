#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "image.h"

_Noreturn void err( const char* msg, ... ) {
    va_list args;
            va_start (args, msg);
    vfprintf(stderr, msg, args);
            va_end (args);
    exit(1);
}

void free_images(struct image* image, struct image* new_image) {
        free(image->data);
        free(new_image->data);
}
