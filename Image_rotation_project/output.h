#ifndef OUTPUT_H
#define OUTPUT_H

#include <stdio.h>

#include "image.h"
#include "bmp.h"


/*  serializer   */
enum  write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_FILE_ERROR,
    WRITE_IMAGE_ERROR
};

enum write_status image_file_convert(const struct image new_image, const char* const new_filename);

#endif // !OUTPUT_H
